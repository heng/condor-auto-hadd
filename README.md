# README
## Introduction
  Name     : Condor Auto hadd monitor (ROOT)                                                                                     
  Function : Make a combination of a large quantity of root file  ==> On Condor                                            
  Author   : Ye Chen （USTC）                                                                                              
                                                                                                                           
  功能： 利用condor合并多个root文件                                                                                        
  作者： 陈烨（USTC）                                                                                                      
                                                                                                                           
## Logic 
  auto_monitor_hadd.sh -> run_condor_hadd.sh -> hadd.sh

## How to run 
    source auto_monitor_hadd.sh
    nohup sh auto_monitor_hadd.sh
    
## auto_monitor_hadd.sh
  README  :                                                                                                                
    1. All you need is to change the variable in "Initialize"                                                              
    2. Default ROOT Setup in file "hadd.sh"                                                                                
    3. If you need to change ROOT version please change "hadd.sh "                                                         
    4. root file combination level in dir: OriginFiles ==>> hadd_level0 ==>> hadd_level1 ==>> hadd_level2 ==>> ......      
    5. Suggest to use "nohup sh auto_monitor_hadd.sh &" to run                                                             
                                                                                                                           
  说明  ：                                                                                                                 
    1. 你只需要更改 “Initialize” 部分                                                                                      
    2. 默认的 ROOT Setup 的环境变量在 “hadd.sh” 中                                                                         
    3. 如果你需要更改ROOT 的环境请更改 "hadd.sh"                                                                           
    4. root file 合并的层级 (Result): OriginFiles ==>> hadd_level0 ==>> hadd_level1 ==>> hadd_level2 ==>> ......                   
    5. 建议运行使用命令"nohup sh auto_monitor_hadd.sh &"                                                                   
                                                                                                                           
## run_condor_hadd.sh
  1.Make condor commit scripts "ff.sub" 

  2.Commit "ff.sub" on condor

## hadd.sh
  1.This is a hadd script runing on condor 

  2.ATLAS ROOT environment setup


